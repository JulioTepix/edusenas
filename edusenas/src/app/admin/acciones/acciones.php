<?php
/*
ini_set('display_errors', 1);
error_reporting(E_ALL);
*/

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    include("../config/config.php");
    $tbl_users = "users";  // Cambié la tabla de empleados a users

    // Variables con los campos de la tabla users
    $name = trim($_POST['name']);
    $email = trim($_POST['email']);
    $password = password_hash(trim($_POST['password']), PASSWORD_BCRYPT);  // Encriptar contraseña
    $created_at = date("Y-m-d H:i:s");  // Fecha actual de creación
    $updated_at = date("Y-m-d H:i:s");

    // Directorio para guardar la imagen del avatar (si aplica)
    $dirLocal = "fotos_usuarios";

    if (isset($_FILES['avatar'])) {
        $archivoTemporal = $_FILES['avatar']['tmp_name'];
        $nombreArchivo = $_FILES['avatar']['name'];

        $extension = strtolower(pathinfo($nombreArchivo, PATHINFO_EXTENSION));

        // Generar un nombre único y seguro para el archivo
        $nombreArchivo = substr(md5(uniqid(rand())), 0, 10) . "." . $extension;
        $rutaDestino = $dirLocal . '/' . $nombreArchivo;

        // Mover el archivo a la ubicación deseada
        if (move_uploaded_file($archivoTemporal, $rutaDestino)) {
            // Insertar el nuevo registro en la tabla users
            $sql = "INSERT INTO $tbl_users (name, email, password, created_at, updated_at, avatar) 
            VALUES ('$name', '$email', '$password', '$created_at', '$updated_at', '$nombreArchivo')";

            if ($conexion->query($sql) === TRUE) {
                header("location:../");
            } else {
                echo "Error al crear el registro: " . $conexion->error;
            }
        } else {
            echo json_encode(array('error' => 'Error al mover el archivo'));
        }
    } else {
        // Si no hay avatar, insertamos sin la imagen
        $sql = "INSERT INTO $tbl_users (name, email, password, created_at, updated_at) 
        VALUES ('$name', '$email', '$password', '$created_at', '$updated_at')";

        if ($conexion->query($sql) === TRUE) {
            header("location:../");
        } else {
            echo "Error al crear el registro: " . $conexion->error;
        }
    }
}

/**
 * Función para obtener todos los usuarios 
 */

function obtenerUsuarios($conexion)
{
    $sql = "SELECT * FROM users ORDER BY id ASC";
    $resultado = $conexion->query($sql);
    if (!$resultado) {
        return false;
    }
    return $resultado;
}
