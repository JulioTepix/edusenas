<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    include("../config/config.php");

    // Leer el cuerpo de la solicitud JSON
    $json_data = file_get_contents("php://input");
    // Decodificar los datos JSON en un array asociativo
    $data = json_decode($json_data, true);

    // Verificar si los datos se decodificaron correctamente
    if ($data !== null) {
        $id = $data['id']; // ID del usuario a eliminar

        // Consulta SQL para eliminar el usuario
        $sql = "DELETE FROM users WHERE id=$id"; // Cambié a la tabla 'users'
        
        if ($conexion->query($sql) === TRUE) {
            echo json_encode(array("success" => true, "message" => "Usuario eliminado correctamente")); // Cambié de empleado a usuario
        } else {
            echo json_encode(array("success" => false, "message" => "Error al eliminar el usuario: " . $conexion->error));
        }
    } else {
        // Si no se proporcionó el parámetro 'id', devolver un mensaje de error
        echo json_encode(array("success" => false, "message" => "No se proporcionó el parámetro 'id'"));
    }
}
