<?php
require_once("../config/config.php");
$id = $_GET['id'];

// Consultar la base de datos para obtener los detalles del usuario
$sql = "SELECT * FROM users WHERE id = $id LIMIT 1"; // Cambié 'tbl_empleados' a 'users'
$query = $conexion->query($sql);
$usuario = $query->fetch_assoc(); // Cambié 'empleado' a 'usuario'

// Devolver los detalles del usuario como un objeto JSON
header('Content-type: application/json; charset=utf-8');
echo json_encode($usuario); // Cambié 'empleado' a 'usuario'
exit;
