<?php
include("../config/config.php");

$fecha_actual = date("Y-m-d");
$filename = "usuarios_" . $fecha_actual . ".csv"; // Cambié de 'empleados_' a 'usuarios_'

// Encabezados para el archivo CSV (sin 'Avatar')
$fields = array('ID', 'Nombre', 'Edad', 'Cédula', 'Sexo', 'Teléfono', 'Cargo');

// Consulta SQL para obtener los datos de los usuarios
$sql = "SELECT * FROM users"; // Cambié 'tbl_empleados' a 'users'
// Ejecutar la consulta
$result = $conexion->query($sql);

// Verificar si hay datos obtenidos de la consulta
if ($result->num_rows > 0) {
    // Abrir el archivo CSV para escritura
    $fp = fopen('php://output', 'w');

    // Agregar los encabezados al archivo CSV
    fputcsv($fp, $fields);

    // Iterar sobre los resultados y agregar cada fila al archivo CSV
    while ($row = $result->fetch_assoc()) {
        // Excluir el campo 'avatar' del CSV
        $csv_row = array($row['id'], $row['nombre'], $row['edad'], $row['cedula'], $row['sexo'], $row['telefono'], $row['cargo']);
        fputcsv($fp, $csv_row);
    }

    // Cerrar el archivo CSV
    fclose($fp);

    // Establecer las cabeceras para descargar el archivo CSV
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '"');

    // Detener la ejecución del script para que solo se descargue el archivo CSV
    exit();
} else {
    // Si no hay datos de usuarios, redireccionar o mostrar un mensaje de error
    echo "No hay usuarios para generar el reporte."; // Cambié de empleados a usuarios
}

// Cerrar la conexión a la base de datos
$conexion->close();
