<?php
if ($_SERVER["REQUEST_METHOD"] == "GET") {
    include("../config/config.php");

    // Obtener el ID de usuario de la solicitud GET y asegurarse de que sea un entero
    $IdUsuario = (int)$_GET['id']; // Cambié de 'IdEmpleado' a 'IdUsuario'

    // Realizar la consulta para obtener los detalles del usuario con el ID proporcionado
    $sql = "SELECT * FROM users WHERE id = $IdUsuario LIMIT 1"; // Cambié 'tbl_empleados' a 'users'
    $resultado = $conexion->query($sql);

    // Verificar si la consulta se ejecutó correctamente
    if (!$resultado) {
        // Manejar el error aquí si la consulta no se ejecuta correctamente
        echo json_encode(["error" => "Error al obtener los detalles del usuario: " . $conexion->error]); // Cambié 'empleado' a 'usuario'
        exit();
    }

    // Obtener los detalles del usuario como un array asociativo
    $usuario = $resultado->fetch_assoc(); // Cambié 'empleado' a 'usuario'

    // Devolver los detalles del usuario como un objeto JSON
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($usuario); // Cambié 'empleado' a 'usuario'
    exit;
}

