<?php
if ($_SERVER["REQUEST_METHOD"] == "GET") {
    include("../config/config.php");

    // Realizar la consulta para obtener los detalles del último usuario registrado
    $sql = "SELECT * FROM users ORDER BY id DESC LIMIT 1"; // Cambié 'tbl_empleados' a 'users'
    $resultado = $conexion->query($sql);

    // Verificar si la consulta se ejecutó correctamente
    if (!$resultado) {
        echo json_encode(["error" => "Error al obtener los detalles del usuario: " . $conexion->error]); // Cambié 'empleado' a 'usuario'
        exit();
    }

    // Obtener los detalles del último usuario registrado, como un array asociativo
    $usuario = $resultado->fetch_assoc(); // Cambié 'empleado' a 'usuario'

    // Devolver los detalles del usuario como un objeto JSON
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($usuario); // Cambié 'empleado' a 'usuario'
    exit;
}
