<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    include("../config/config.php");

    $id = trim($_POST['id']); // Asegúrate de recibir el ID del usuario que se actualizará
    $name = trim($_POST['name']);
    $email = trim($_POST['email']);
    $password = trim($_POST['password']); // Si deseas cambiar la contraseña
    $hashed_password = null;

    // Si hay una nueva contraseña, encriptarla
    if (!empty($password)) {
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);
    }

    // Actualiza los datos en la base de datos
    $sql = "UPDATE users SET name='$name', email='$email'";

    // Si hay un nuevo password, actualiza su valor
    if ($hashed_password !== null) {
        $sql .= ", password='$hashed_password'";
    }

    $sql .= " WHERE id='$id'";

    if ($conexion->query($sql) === TRUE) {
        header("location:../");
    } else {
        echo "Error al actualizar el registro: " . $conexion->error;
    }
}
?>
