/**
 * Modal para agregar un nuevo usuario
 */
async function modalRegistrarUsuario() {
  try {
    // Ocultar la modal si está abierta
    const existingModal = document.getElementById("detalleUsuarioModal");
    if (existingModal) {
      const modal = bootstrap.Modal.getInstance(existingModal);
      if (modal) {
        modal.hide();
      }
      existingModal.remove(); // Eliminar la modal existente
    }

    const response = await fetch("./admin/modales/modalAdd.php");

    if (!response.ok) {
      throw new Error("Error al cargar la modal");
    }

    // Obtener el contenido de la modal
    const data = await response.text();

    // Crear un elemento div para almacenar el contenido de la modal
    const modalContainer = document.createElement("div");
    modalContainer.innerHTML = data;

    // Agregar la modal al documento actual
    document.body.appendChild(modalContainer);

    // Mostrar la modal
    const myModal = new bootstrap.Modal(
      modalContainer.querySelector("#agregarUsuarioModal")
    );
    myModal.show();
  } catch (error) {
    console.error(error);
  }
}

/**
 * Función para enviar el formulario al backend
 */
async function registrarUsuario(event) {
  try {
    event.preventDefault(); // Evitar que la página se recargue al enviar el formulario

    const formulario = document.querySelector("#formularioUsuario"); // Asegúrate de que este ID esté en tu modal
    const formData = new FormData(formulario); // Crear un objeto FormData

    // Enviar los datos del formulario al backend usando Axios
    const response = await axios.post("acciones/acciones.php", formData);

    // Verificar la respuesta del backend
    if (response.status === 200) {
      // Llamar a la función para actualizar la tabla de usuarios
      window.insertUsuarioTable();

      setTimeout(() => {
        $("#agregarUsuarioModal").css("opacity", "");
        $("#agregarUsuarioModal").modal("hide");

        // Llamar a la función para mostrar un mensaje de éxito
        toastr.options = window.toastrOptions;
        toastr.success("¡El usuario se registró correctamente!");
      }, 600);
    } else {
      console.error("Error al registrar el usuario");
    }
  } catch (error) {
    console.error("Error al enviar el formulario", error);
  }
}