/**
 * Función para mostrar la modal de detalles del usuario
 */
async function verDetallesUsuario(idUsuario) {
  try {
    // Ocultar la modal si está abierta
    const existingModal = document.getElementById("detalleUsuarioModal");
    if (existingModal) {
      const modal = bootstrap.Modal.getInstance(existingModal);
      if (modal) {
        modal.hide();
      }
      existingModal.remove(); // Eliminar la modal existente
    }

    // Buscar la Modal de Detalles
    const response = await fetch("modales/modalDetalles.php");
    if (!response.ok) {
      throw new Error("Error al cargar la modal de detalles del usuario");
    }
    const modalHTML = await response.text();

    // Crear un elemento div para almacenar el contenido de la modal
    const modalContainer = document.createElement("div");
    modalContainer.innerHTML = modalHTML;

    // Agregar la modal al documento actual
    document.body.appendChild(modalContainer);

    // Mostrar la modal
    const myModal = new bootstrap.Modal(
      modalContainer.querySelector("#detalleUsuarioModal")
    );
    myModal.show();

    await cargarDetalleUsuario(idUsuario);
  } catch (error) {
    console.error(error);
  }
}

/**
 * Función para cargar y mostrar los detalles del usuario en la modal
 */
async function cargarDetalleUsuario(idUsuario) {
  try {
    const response = await axios.get(
      `acciones/detallesEmpleado.php?id=${idUsuario}`
    );
    if (response.status === 200) {
      console.log(response.data);
      const { name, email, created_at } = response.data;

      // Limpiar el contenido existente de la lista ul
      const ulDetalleUsuario = document.querySelector(
        "#detalleEmpleadoContenido ul"
      );

      ulDetalleUsuario.innerHTML = ` 
        <li class="list-group-item"><b>Nombre:</b> 
          ${name ? name : "No disponible"}
        </li>
        <li class="list-group-item"><b>Email:</b> 
          ${email ? email : "No disponible"}
        </li>
        <li class="list-group-item"><b>Fecha de Creación:</b> 
          ${created_at ? created_at : "No disponible"}
        </li>
      `;
    } else {
      alert(`Error al cargar los detalles del usuario con ID ${idUsuario}`);
    }
  } catch (error) {
    console.error(error);
    alert("Hubo un problema al cargar los detalles del usuario");
  }
}
