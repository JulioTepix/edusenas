/**
 * Función para mostrar la modal de editar el usuario
 */
async function editarUsuario(idUsuario) {
  try {
    // Ocultar la modal si está abierta
    const existingModal = document.getElementById("editarUsuarioModal");
    if (existingModal) {
      const modal = bootstrap.Modal.getInstance(existingModal);
      if (modal) {
        modal.hide();
      }
      existingModal.remove(); // Eliminar la modal existente
    }

    const response = await fetch("modales/modalEditar.php");
    if (!response.ok) {
      throw new Error("Error al cargar la modal de editar el usuario");
    }
    const modalHTML = await response.text();

    // Crear un elemento div para almacenar el contenido de la modal
    const modalContainer = document.createElement("div");
    modalContainer.innerHTML = modalHTML;

    // Agregar la modal al documento actual
    document.body.appendChild(modalContainer);

    // Mostrar la modal
    const myModal = new bootstrap.Modal(
      modalContainer.querySelector("#editarUsuarioModal")
    );
    myModal.show();

    await cargarDatosUsuarioEditar(idUsuario);
  } catch (error) {
    console.error(error);
  }
}

/**
 * Función buscar información del usuario seleccionado y cargarla en la modal
 */
async function cargarDatosUsuarioEditar(idUsuario) {
  try {
    const response = await axios.get(
      `acciones/detallesEmpleado.php?id=${idUsuario}`
    );
    if (response.status === 200) {
      const { id, name, email } = response.data;

      console.log(id, name, email);
      document.querySelector("#idusuario").value = id;
      document.querySelector("#nombre").value = name;
      document.querySelector("#email").value = email;

    } else {
      console.log("Error al cargar el usuario a editar");
    }
  } catch (error) {
    console.error(error);
    alert("Hubo un problema al cargar los detalles del usuario");
  }
}

async function actualizarUsuario(event) {
  try {
    event.preventDefault();

    const formulario = document.querySelector("#formularioUsuarioEdit");
    // Crear un objeto FormData para enviar los datos del formulario
    const formData = new FormData(formulario);
    const idusuario = formData.get("id");

    // Enviar los datos del formulario al backend usando Axios
    const response = await axios.post("acciones/updateEmpleado.php", formData);

    // Verificar la respuesta del backend
    if (response.status === 200) {
      console.log("Usuario actualizado exitosamente");

      // Llamar a la función para actualizar la tabla de usuarios
      window.actualizarUsuarioEdit(idusuario);

      //Llamar a la función para mostrar un mensaje de éxito
      if (window.toastrOptions) {
        toastr.options = window.toastrOptions;
        toastr.success("¡El usuario se actualizó correctamente!.");
      }

      setTimeout(() => {
        $("#editarUsuarioModal").css("opacity", "");
        $("#editarUsuarioModal").modal("hide");
      }, 600);
    } else {
      console.error("Error al actualizar el usuario");
    }
  } catch (error) {
    console.error("Error al enviar el formulario", error);
  }
}
