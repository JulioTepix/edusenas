/**
 * Modal para confirmar la eliminación de un usuario
 */
async function cargarModalConfirmacion() {
  try {
    const existingModal = document.getElementById("editarUsuarioModal");
    if (existingModal) {
      const modal = bootstrap.Modal.getInstance(existingModal);
      if (modal) {
        modal.hide();
      }
      existingModal.remove(); // Eliminar la modal existente
    }

    // Realizar una solicitud GET usando Fetch para obtener el contenido de la modal
    const response = await fetch("modales/modalDelete.php");

    if (!response.ok) {
      throw new Error("Error al cargar la modal de confirmación");
    }

    // Obtener el contenido de la modal
    const modalHTML = await response.text();

    // Crear un elemento div para almacenar el contenido de la modal
    const modalContainer = document.createElement("div");
    modalContainer.innerHTML = modalHTML;

    // Agregar la modal al documento actual
    document.body.appendChild(modalContainer);

    // Mostrar la modal
    const myModal = new bootstrap.Modal(modalContainer.querySelector(".modal"));
    myModal.show();
  } catch (error) {
    console.error(error);
  }
}

/**
 * Función para eliminar un usuario desde la modal
 */
async function eliminarUsuario(idUsuario) {
  try {
    // Llamar a la función para cargar y mostrar la modal de confirmación
    await cargarModalConfirmacion();

    // Establecer el ID del usuario en el botón de confirmación
    document
      .getElementById("confirmDeleteBtn")
      .setAttribute("data-id", idUsuario);

    // Agregar un event listener al botón "Eliminar usuario"
    document
      .getElementById("confirmDeleteBtn")
      .addEventListener("click", async function () {
        // Obtener el ID del usuario a eliminar
        var idUsuario = this.getAttribute("data-id");

        try {
          const response = await axios.post("acciones/delete.php", {
            id: idUsuario,
          });

          if (response.status === 200) {
            // Eliminar la fila correspondiente a este usuario de la tabla
            document.querySelector(`#usuario_${idUsuario}`).remove();
            // Llamar a la función para mostrar un mensaje de éxito
            if (window.toastrOptions) {
              toastr.options = window.toastrOptions;
              toastr.error("¡El usuario se eliminó correctamente!.");
            }
          } else {
            alert(`Error al eliminar el usuario con ID ${idUsuario}`);
          }
        } catch (error) {
          console.error(error);
          alert("Hubo un problema al eliminar al usuario");
        } finally {
          // Cerrar la modal de confirmación
          var confirmModal = bootstrap.Modal.getInstance(
            document.getElementById("confirmModal")
          );
          confirmModal.hide();
        }
      });
  } catch (error) {
    console.error(error);
    alert("Hubo un problema al cargar la modal de confirmación");
  }
}
