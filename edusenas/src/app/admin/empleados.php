<div class="table-responsive">
    <table class="table table-hover" id="table_usuarios"> <!-- Cambié de empleados a usuarios -->
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Email</th> <!-- Cambié Edad a Email -->
                <th scope="col">Fecha de Creación</th> <!-- Cambié Cédula a Fecha de Creación -->
                <th scope="col">Acciones</th> <!-- Quité el avatar -->
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($usuarios as $usuario) { ?> <!-- Cambié empleados a usuarios -->
                <tr id="usuario_<?php echo $usuario['id']; ?>"> <!-- Cambié empleado a usuario -->
                    <th scope='row'><?php echo $usuario['id']; ?></th>
                    <td><?php echo $usuario['name']; ?></td> <!-- Cambié nombre a name -->
                    <td><?php echo $usuario['email']; ?></td> <!-- Cambié edad a email -->
                    <td><?php echo $usuario['created_at']; ?></td> <!-- Cambié cedula a created_at -->
                    <td>
                        <a title="Ver detalles del usuario" href="#" onclick="verDetallesUsuario(<?php echo $usuario['id']; ?>)" class="btn btn-success"> <!-- Cambié empleado a usuario -->
                            <i class="bi bi-binoculars"></i>
                        </a>
                        <a title="Editar datos del usuario" href="#" onclick="editarUsuario(<?php echo $usuario['id']; ?>)" class="btn btn-warning"> <!-- Cambié empleado a usuario -->
                            <i class="bi bi-pencil-square"></i>
                        </a>
                        <a title="Eliminar datos del usuario" href="#" onclick="eliminarUsuario(<?php echo $usuario['id']; ?>)" class="btn btn-danger"> <!-- Cambié empleado a usuario -->
                            <i class="bi bi-trash"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
