<div class="modal fade" id="editarUsuarioModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5 titulo_modal">Actualizar Información del Usuario</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="formularioUsuarioEdit" action="" method="POST" enctype="multipart/form-data" autocomplete="off">
                    <input type="hidden" name="id" id="idusuario" />
                    
                    <div class="mb-3">
                        <label class="form-label">Nombre</label>
                        <input type="text" name="nombre" id="nombre" class="form-control" required />
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Email</label>
                        <input type="email" name="email" id="email" class="form-control" required />
                    </div>

                
                    <div class="mb-3 mt-4">
                        <label class="form-label">Foto actual del usuario</label>
                        <br>
                        <img src="" id="avatar" style="display: block;" class="rounded-circle float-start" alt="Foto del usuario" width="80">
                    </div>
                    <br> <br>

                    <div class="mb-3 mt-4">
                        <label class="form-label">Cambiar Foto del usuario</label>
                        <input class="form-control form-control-sm" type="file" name="avatar" accept="image/png, image/jpeg" />
                    </div>

                    <div class="d-grid gap-2">
                        <button type="submit" class="btn btn-primary btn_add" onclick="actualizarUsuario(event)">
                            Actualizar datos del usuario
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
