import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'; // Asegúrate de importar HttpClientModule
import { CommonModule } from '@angular/common'; // Importa CommonModule
import { AppComponent } from './app.component';
import { ProfileComponent } from './profile/profile.component'; // Asegúrate de que la ruta sea correcta

@NgModule({
  declarations: [
    ProfileComponent // Asegúrate de que esté declarado aquí
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CommonModule // Asegúrate de que CommonModule esté aquí
  ],
  providers: [],
  bootstrap: []
})
export class AppModule { }