import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

interface PredictionData {
  label: string;
  confidence: number;
}

@Component({
  selector: 'app-image',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {
  @ViewChild('videoElement', { static: true }) videoElement!: ElementRef;
  @ViewChild('canvasElement', { static: true }) canvasElement!: ElementRef;
  selectedFile: File | null = null;
  prediction: string | null = null;
  error: string | null = null;
  percentage: number | null = 0;
  imageUrl: string | null = null;
  stream: MediaStream | null = null;
  letra: string | null = null;

  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.requestCameraPermissions();
    this.letra = this.route.snapshot.paramMap.get('letra');
  }

  async requestCameraPermissions() {
    try {
      const stream = await navigator.mediaDevices.getUserMedia({ video: true });
      this.stream = stream;
      this.videoElement.nativeElement.srcObject = stream;
    } catch (error) {
      console.error('Error al solicitar permisos de cámara:', error);
    }
  }

  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
    if (this.selectedFile) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.imageUrl = e.target.result;
      };
      reader.readAsDataURL(this.selectedFile);
    }
  }

  reload(){
    this.requestCameraPermissions();
    const context = this.canvasElement.nativeElement.getContext('2d');
    context.clearRect(0, 0, this.canvasElement.nativeElement.width, this.canvasElement.nativeElement.height);
  }

  onUpload() {
    const context = this.canvasElement.nativeElement.getContext('2d');
    context.drawImage(this.videoElement.nativeElement, 0, 0, this.canvasElement.nativeElement.width, this.canvasElement.nativeElement.height);

    // Obtén la imagen como base64
    const photoDataUrl = this.canvasElement.nativeElement.toDataURL('image/jpeg');

    // Convierte la imagen base64 a Blob
    const byteString = atob(photoDataUrl.split(',')[1]);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const intArray = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      intArray[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([intArray], { type: 'image/jpeg' });
    const file = new File([blob], 'captured_image.jpg', { type: 'image/jpeg' });

    // Muestra la imagen en la página (puedes usar una <img> o cualquier otro elemento)

    const formData = new FormData();
    formData.append('file', file, file.name);

    this.http.post<PredictionData>('http://127.0.0.1:5000/predict', formData)
      .subscribe({
        next: (data: PredictionData) => {
          const label = data.label; // Accediendo a la propiedad 'label'
          const confidence = data.confidence; // Accediendo a la propiedad 'confidence'
          if(this.letra == label){
            this.percentage = parseFloat((data.confidence * 100).toFixed(2));
            this.prediction = `Seña correcta`;
          }else{
            this.percentage = 0;
          this.prediction = `Seña no detectada`;
          }
          console.log(this.letra, label);
          this.error = null;
        },
        error: err => {
          this.error = err;
          this.prediction = null;
        }
      });

    this.videoElement.nativeElement.srcObject = null;
    if (this.stream) {
      this.stream.getTracks().forEach(track => track.stop());
      this.stream = null;
    }
  }
}
