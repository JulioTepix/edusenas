import { Component, OnInit, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as bootstrap from 'bootstrap';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit, AfterViewInit {
  title: string = '';

  constructor(private http: HttpClient) { }

  showTitle(name: string) {
    this.title = name;
  }

  hideTitle() {
    this.title = '';
  }

  ngOnInit(): void {
    console.log('El componente se ha inicializado.');

    // Cargar datos desde la API
    this.http.get('http://127.0.0.1:5000/api/v1/users')
      .subscribe({
        next: data => {
          console.log('Éxito:', data);
        },
        error: err => {
          console.error('Error:', err);
        }
      });
  }

  ngAfterViewInit(): void {
    this.configureAccordion();
  }

  configureAccordion(): void {
    const accordionButtons = document.querySelectorAll('.accordion-button');

    accordionButtons.forEach(button => {
      button.addEventListener('click', () => {
        // Cerrar todas las secciones del acordeón
        accordionButtons.forEach(btn => {
          if (btn !== button) {
            const targetSelector = btn.getAttribute('data-bs-target');
            if (targetSelector) {
              const targetCollapse = document.querySelector(targetSelector);
              if (targetCollapse) {
                const bootstrapCollapse = new bootstrap.Collapse(targetCollapse, { toggle: false });
                bootstrapCollapse.hide();
              }
            }
          }
        });
      });
    });
  }
}
