import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  // Datos del usuario (puedes reemplazarlo con datos que provengan de tu backend)
  usuario = {
    nombre: 'Juan Pérez',
    correo: 'juan.perez@example.com',
    imagenPerfil: 'assets/default-profile.png'  // Imagen por defecto
  };

  constructor() { }

  ngOnInit(): void {
    // Aquí podrías cargar los datos del usuario desde un servicio
  }

  // Método para manejar la carga de la imagen
  onFileSelected(event: any): void {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.usuario.imagenPerfil = e.target.result;  // Actualizar la imagen del perfil
      };
      reader.readAsDataURL(file);
    }
  }
}
