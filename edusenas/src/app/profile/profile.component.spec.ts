// src/app/profile/profile.component.ts
import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  userProfile: any = {}; // Almacena la información del usuario

  constructor(private userService: UserService, private authService: AuthService) {}

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      this.userService.getUserProfile().subscribe(
        (data) => {
          this.userProfile = data; // Guarda los datos del perfil
        },
        (error) => {
          console.error('Error fetching user data', error);
        }
      );
    } else {
      console.error('Usuario no autenticado'); // Maneja el caso cuando el usuario no está autenticado
    }
  }

  get isAuthenticated(): boolean {
    return this.authService.isAuthenticated();
  }
}
