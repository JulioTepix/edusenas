// src/app/services/auth.service.ts
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private token: string | null = null; // Aquí podrías guardar el token

  constructor() {
    // Carga el token del almacenamiento local al inicializar
    this.token = localStorage.getItem('token');
  }

  login(token: string) {
    this.token = token; // Guarda el token
    localStorage.setItem('token', token); // Guarda en almacenamiento local
  }

  isAuthenticated(): boolean {
    return !!this.token; // Devuelve true si hay un token
  }

  logout() {
    this.token = null; // Borra el token
    localStorage.removeItem('token'); // Elimina del almacenamiento local
  }
}
