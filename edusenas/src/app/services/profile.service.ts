import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private apiUrl = 'http://localhost:8000/api/profile'; // Asegúrate de que la URL sea correcta

  constructor(private http: HttpClient) {}

  getProfile(): Observable<any> {
    return this.http.get<any>(this.apiUrl);
  }

  updateProfileImage(formData: FormData): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/update-image`, formData);
  }
}
