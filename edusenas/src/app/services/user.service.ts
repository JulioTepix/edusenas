// src/app/services/user.service.ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs'; // Asegúrate de importar throwError
import { catchError } from 'rxjs/operators'; // Asegúrate de importar catchError


@Injectable({
  providedIn: 'root',
})
export class UserService {
  private apiUrl = 'http://localhost:8000/api'; // URL de tu API en Laravel

  constructor(private http: HttpClient) {}

  getUserProfile(): Observable<any> {
    return this.http.get<any>(this.apiUrl).pipe(
      catchError(error => {
        console.error('Error al obtener el perfil:', error);
        return throwError(error);
      })
    );
  }
}