import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private apiUrl = 'http://127.0.0.1:8000/api/profile'; // Ajusta esto según tu configuración de API

  constructor(private http: HttpClient) {}

  getProfile(): Observable<any> {
    // Obtener el token de autenticación del almacenamiento local
    const token = localStorage.getItem('token'); // Cambia esto según tu lógica

    // Configurar los encabezados para incluir el token
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });

    return this.http.get(this.apiUrl, { headers }); // Agrega los encabezados a la solicitud
  }

  updateProfileImage(formData: FormData): Observable<any> {
    const token = localStorage.getItem('token'); // Obtener el token

    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });

    return this.http.post(`${this.apiUrl}/upload-image`, formData, { headers }); // Incluye los encabezados
  }
}
