<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User; // Asegúrate de que estás importando el modelo correcto

class ProfileController extends Controller
{
    public function showProfile()
    {
        try {
            $user = Auth::user(); // Obtiene el usuario autenticado
            return response()->json($user);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Error fetching profile: ' . $e->getMessage()], 500);
        }
    }

    public function updateProfileImage(Request $request)
    {
        $request->validate([
            'profile_image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        $user = Auth::user();
        
        if ($request->hasFile('profile_image')) {
            $imageName = time().'.'.$request->profile_image->extension();  
            $request->profile_image->move(public_path('images'), $imageName);

            // Actualiza la ruta de la imagen en el usuario
            $user->profile_image = $imageName;
            $user->save();
        }

        return response()->json(['success' => 'Imagen de perfil actualizada']);
    }
}
