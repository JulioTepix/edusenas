<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function profile(Request $request)
    {
        // Obtén el usuario autenticado
        $user = Auth::user();

        // Devuelve la información del usuario como un JSON
        return response()->json([
            'name' => $user->name,
            'email' => $user->email,
        ]);
    }
}
