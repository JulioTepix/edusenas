<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Sanctum\Sanctum;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // Aquí puedes registrar tus políticas si las tienes
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        $this->registerPolicies();

        // Opcional: Si necesitas personalizar la autorización
        // Gate::define('view-profile', function ($user) {
        //     return true; // Cambia esto según tus reglas de autorización
        // });

        //Sanctum::usePersonalAccessTokens();
    }
}
