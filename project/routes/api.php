<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ProfileController; // Asegúrate de importar el ProfileController
use App\Models\Client;
use App\Models\User; // Asegúrate de que sea 'User', no 'Users'
use App\Http\Controllers\UserController;


/*
|-------------------------------------------------------------------------- 
| API Routes 
|-------------------------------------------------------------------------- 
| 
| Here is where you can register API routes for your application. These 
| routes are loaded by the RouteServiceProvider and all of them will 
| be assigned to the "api" middleware group. Make something great! 
| 
*/

// Rutas de autenticación
Route::controller(AuthController::class)->group(function() {
    Route::post('/register', 'register');
    Route::post('/login', 'login');
    Route::get('/users', 'index');
});

// Rutas del cliente
Route::controller(ClientController::class)->group(function() {
    Route::post('/add', 'add');
    Route::get('/clients', 'index');
});

// Rutas de perfil
Route::middleware('auth:sanctum')->get('/profile', [ProfileController::class, 'showProfile']);
Route::middleware('auth:sanctum')->post('/profile/image', [ProfileController::class, 'updateProfileImage']);
Route::middleware('auth:sanctum')->get('/user', [UserController::class, 'getUser']);
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware('auth:sanctum')->get('/user/profile', [UserController::class, 'profile']);
